--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases
--

DROP DATABASE "FODB";




--
-- Drop roles
--

DROP ROLE andy;


--
-- Roles
--

CREATE ROLE andy;
ALTER ROLE andy WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md5d163264b5fb3e8d2c232d1fb780921bc';






--
-- Database creation
--

CREATE DATABASE "FODB" WITH TEMPLATE = template0 OWNER = andy;
REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


\connect "FODB"

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-1.pgdg90+1)
-- Dumped by pg_dump version 10.5 (Debian 10.5-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: inspections_check_prerequisites(); Type: FUNCTION; Schema: public; Owner: andy
--

CREATE FUNCTION public.inspections_check_prerequisites() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
  RETURN NEW;
 END;
$$;


ALTER FUNCTION public.inspections_check_prerequisites() OWNER TO andy;

--
-- Name: inspections_compare_score(); Type: FUNCTION; Schema: public; Owner: andy
--

CREATE FUNCTION public.inspections_compare_score() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
  IF (NEW.score = OLD.score) THEN
   RETURN NEW;
  END IF;
  RAISE EXCEPTION 'Updates to inspection score forbidden.';
 END;
$$;


ALTER FUNCTION public.inspections_compare_score() OWNER TO andy;

--
-- Name: inspections_count_inspectors(); Type: FUNCTION; Schema: public; Owner: andy
--

CREATE FUNCTION public.inspections_count_inspectors() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
  RETURN NEW;
 END;
$$;


ALTER FUNCTION public.inspections_count_inspectors() OWNER TO andy;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: addresses; Type: TABLE; Schema: public; Owner: andy
--

CREATE TABLE public.addresses (
    id integer NOT NULL,
    street_address text NOT NULL,
    city text NOT NULL,
    state text NOT NULL,
    zip_code character varying(10) NOT NULL
);


ALTER TABLE public.addresses OWNER TO andy;

--
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: andy
--

CREATE SEQUENCE public.addresses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.addresses_id_seq OWNER TO andy;

--
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: andy
--

ALTER SEQUENCE public.addresses_id_seq OWNED BY public.addresses.id;


--
-- Name: builders; Type: TABLE; Schema: public; Owner: andy
--

CREATE TABLE public.builders (
    license integer NOT NULL,
    name text NOT NULL,
    address_id integer NOT NULL,
    CONSTRAINT valid_license CHECK (((license >= 10000) AND (license <= 99999)))
);


ALTER TABLE public.builders OWNER TO andy;

--
-- Name: inspection_sites; Type: TABLE; Schema: public; Owner: andy
--

CREATE TABLE public.inspection_sites (
    id integer NOT NULL,
    builder_id integer NOT NULL,
    address_id integer NOT NULL,
    type text NOT NULL,
    size integer,
    first_activity_date date
);


ALTER TABLE public.inspection_sites OWNER TO andy;

--
-- Name: inspection_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: andy
--

CREATE SEQUENCE public.inspection_sites_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inspection_sites_id_seq OWNER TO andy;

--
-- Name: inspection_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: andy
--

ALTER SEQUENCE public.inspection_sites_id_seq OWNED BY public.inspection_sites.id;


--
-- Name: inspection_types; Type: TABLE; Schema: public; Owner: andy
--

CREATE TABLE public.inspection_types (
    code character varying(3) NOT NULL,
    description text NOT NULL,
    prerequisites text[] NOT NULL,
    cost integer NOT NULL
);


ALTER TABLE public.inspection_types OWNER TO andy;

--
-- Name: inspections; Type: TABLE; Schema: public; Owner: andy
--

CREATE TABLE public.inspections (
    id integer NOT NULL,
    inspection_date date NOT NULL,
    type character varying(3) NOT NULL,
    inspector_id integer NOT NULL,
    score integer NOT NULL,
    notes text,
    address_id integer NOT NULL,
    CONSTRAINT valid_score CHECK (((score >= 0) AND (score <= 100)))
);


ALTER TABLE public.inspections OWNER TO andy;

--
-- Name: inspections_id_seq; Type: SEQUENCE; Schema: public; Owner: andy
--

CREATE SEQUENCE public.inspections_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inspections_id_seq OWNER TO andy;

--
-- Name: inspections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: andy
--

ALTER SEQUENCE public.inspections_id_seq OWNED BY public.inspections.id;


--
-- Name: inspectors; Type: TABLE; Schema: public; Owner: andy
--

CREATE TABLE public.inspectors (
    id integer NOT NULL,
    name text NOT NULL,
    hire_date date NOT NULL,
    CONSTRAINT valid_id CHECK (((id >= 0) AND (id <= 99999)))
);


ALTER TABLE public.inspectors OWNER TO andy;

--
-- Name: pending_inspections; Type: TABLE; Schema: public; Owner: andy
--

CREATE TABLE public.pending_inspections (
    id integer NOT NULL,
    inspection_date date NOT NULL,
    type character varying(3) NOT NULL,
    inspection_site_id integer NOT NULL
);


ALTER TABLE public.pending_inspections OWNER TO andy;

--
-- Name: pending_inspections_id_seq; Type: SEQUENCE; Schema: public; Owner: andy
--

CREATE SEQUENCE public.pending_inspections_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pending_inspections_id_seq OWNER TO andy;

--
-- Name: pending_inspections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: andy
--

ALTER SEQUENCE public.pending_inspections_id_seq OWNED BY public.pending_inspections.id;


--
-- Name: addresses id; Type: DEFAULT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.addresses ALTER COLUMN id SET DEFAULT nextval('public.addresses_id_seq'::regclass);


--
-- Name: inspection_sites id; Type: DEFAULT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspection_sites ALTER COLUMN id SET DEFAULT nextval('public.inspection_sites_id_seq'::regclass);


--
-- Name: inspections id; Type: DEFAULT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspections ALTER COLUMN id SET DEFAULT nextval('public.inspections_id_seq'::regclass);


--
-- Name: pending_inspections id; Type: DEFAULT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.pending_inspections ALTER COLUMN id SET DEFAULT nextval('public.pending_inspections_id_seq'::regclass);


--
-- Data for Name: addresses; Type: TABLE DATA; Schema: public; Owner: andy
--

COPY public.addresses (id, street_address, city, state, zip_code) FROM stdin;
1	100 Winding Wood	Carrollton	TX	75006
2	308 Oak St.	Dallas	TX	75219
3	306 Oak St.	Dallas	TX	75219
4	212 Cherry Bark Lane	Plano	TX	75074
5	100 Industrial Ave.	Fort Worth	TX	76104
6	103 Industrial Ave.	Fort Worth	TX	76104
7	104 Industrial Ave.	Fort Worth	TX	76104
8	304 Oak St.	Dallas	TX	75219
9	100 Main St.	Dallas	TX	75226
10	102 Winding Wood	Carrollton	TX	75006
11	300 Oak St.	Dallas	TX	75219
12	216 Cherry Bark Lane	Plano	TX	75074
13	302 Oak St.	Dallas	TX	75219
14	101 Industrial Ave.	Fort Worth	TX	76104
15	210 Cherry Bark Lane	Plano	TX	75074
16	105 Industrial Ave.	Fort Worth	TX	76104
17	214 Cherry Bark Lane	Plano	TX	75074
18	102 Industrial Ave.	Fort Worth	TX	76104
19	6300 N Central Expy.	Dallas	TX	75206
20	1728 Briercroft Ct.	Carrollton	TX	75006
21	788 E Collins Blvd.	Richardson	TX	75081
22	1901 Regal Row	Dallas	TX	75235
23	5101 Maple Ave.	Dallas	TX	75235
\.


--
-- Data for Name: builders; Type: TABLE DATA; Schema: public; Owner: andy
--

COPY public.builders (license, name, address_id) FROM stdin;
12345	Builder-1	21
23456	Builder-2	23
34567	Builder-3	22
45678	Builder-4	19
12321	Builder-5	20
\.


--
-- Data for Name: inspection_sites; Type: TABLE DATA; Schema: public; Owner: andy
--

COPY public.inspection_sites (id, builder_id, address_id, type, size, first_activity_date) FROM stdin;
1	12345	9	commercial	250000	1999-12-31
2	12345	11	residential	3000	2000-01-01
3	12345	13	residential	4000	2001-02-01
4	12345	8	residential	1500	2002-03-01
5	12345	3	residential	1500	2003-04-01
6	12345	2	residential	2000	2003-04-01
7	23456	5	commercial	100000	2005-06-01
8	23456	14	commercial	80000	2005-06-01
9	23456	18	commercial	75000	2005-06-01
10	23456	6	commercial	50000	2005-06-01
11	23456	7	commercial	80000	2005-06-01
12	23456	16	commercial	90000	2005-06-01
13	45678	1	residential	2500	\N
14	45678	10	residential	2800	\N
15	12321	15	residential	3200	2016-10-01
16	12321	4	residential	\N	\N
17	12321	17	residential	\N	\N
18	12321	12	residential	\N	\N
\.


--
-- Data for Name: inspection_types; Type: TABLE DATA; Schema: public; Owner: andy
--

COPY public.inspection_types (code, description, prerequisites, cost) FROM stdin;
FRM	Framing	{}	100
PLU	Plumbing	{FRM}	100
POL	Pool	{PLU}	50
ELE	Electrical	{FRM}	100
SAF	Safety	{}	50
HAC	Heating/Cooling	{ELE}	100
FNL	Final	{HAC,PLU}	200
FN2	Final - 2 needed	{ELE,PLU}	150
FN3	Final - plumbing	{PLU}	150
HIS	Historical accuracy	{}	100
\.


--
-- Data for Name: inspections; Type: TABLE DATA; Schema: public; Owner: andy
--

COPY public.inspections (id, inspection_date, type, inspector_id, score, notes, address_id) FROM stdin;
1	2018-10-01	FRM	103	100	no problems noted	1
2	2018-10-01	FRM	101	100	no problems noted	11
3	2018-10-01	FRM	102	100	no problems noted	13
4	2018-10-02	PLU	101	90	minor leak, corrected	11
5	2018-10-02	PLU	102	25	massive leaks	13
6	2018-10-03	ELE	101	80	exposed junction box	11
7	2018-10-04	HAC	101	80	duct needs taping	11
8	2018-10-05	FNL	101	90	ready for owner	11
9	2018-10-08	PLU	102	50	still leaking	13
10	2018-10-12	FRM	103	85	no issues but messy	15
11	2018-10-12	PLU	102	80	no leaks, but messy	13
12	2018-10-14	ELE	104	100	no problems noted	15
13	2018-10-14	SAF	102	100	no problems noted	13
14	2018-10-20	PLU	103	100	everything working	1
15	2018-10-25	ELE	103	100	no problems noted	1
16	2018-11-01	FRM	103	100	no problems noted	10
17	2018-11-01	HAC	102	80	duct needs taping	13
18	2018-11-02	HAC	103	100	no problems noted	1
19	2018-11-02	PLU	103	90	minor leak, corrected	10
20	2018-11-02	FRM	105	100	tbd	16
21	2018-11-02	FNL	102	90	ready for owner	13
22	2018-11-03	ELE	103	80	exposed junction box	10
23	2018-11-04	PLU	103	80	duct needs sealing	15
24	2018-11-05	POL	105	90	ready for owner	15
25	2018-11-06	FRM	105	100	okay	5
26	2018-11-08	PLU	102	100	no leaks	5
27	2018-11-12	POL	102	80	pool equipment okay	5
28	2018-11-14	FN3	103	100	no problems noted	5
\.


--
-- Data for Name: inspectors; Type: TABLE DATA; Schema: public; Owner: andy
--

COPY public.inspectors (id, name, hire_date) FROM stdin;
101	Inspector-1	1984-11-08
102	Inspector-2	1994-11-08
103	Inspector-3	2004-11-08
104	Inspector-4	2014-11-01
105	Inspector-5	2018-11-01
\.


--
-- Data for Name: pending_inspections; Type: TABLE DATA; Schema: public; Owner: andy
--

COPY public.pending_inspections (id, inspection_date, type, inspection_site_id) FROM stdin;
1	2018-09-01	FNL	16
2	2018-10-26	FRM	4
3	2018-11-04	PLU	4
\.


--
-- Name: addresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: andy
--

SELECT pg_catalog.setval('public.addresses_id_seq', 23, true);


--
-- Name: inspection_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: andy
--

SELECT pg_catalog.setval('public.inspection_sites_id_seq', 18, true);


--
-- Name: inspections_id_seq; Type: SEQUENCE SET; Schema: public; Owner: andy
--

SELECT pg_catalog.setval('public.inspections_id_seq', 28, true);


--
-- Name: pending_inspections_id_seq; Type: SEQUENCE SET; Schema: public; Owner: andy
--

SELECT pg_catalog.setval('public.pending_inspections_id_seq', 3, true);


--
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: builders builders_pkey; Type: CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.builders
    ADD CONSTRAINT builders_pkey PRIMARY KEY (license);


--
-- Name: inspection_sites inspection_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspection_sites
    ADD CONSTRAINT inspection_sites_pkey PRIMARY KEY (id);


--
-- Name: inspection_types inspection_types_pkey; Type: CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspection_types
    ADD CONSTRAINT inspection_types_pkey PRIMARY KEY (code);


--
-- Name: inspections inspections_pkey; Type: CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspections
    ADD CONSTRAINT inspections_pkey PRIMARY KEY (id);


--
-- Name: inspectors inspectors_pkey; Type: CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspectors
    ADD CONSTRAINT inspectors_pkey PRIMARY KEY (id);


--
-- Name: pending_inspections pending_inspections_pkey; Type: CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.pending_inspections
    ADD CONSTRAINT pending_inspections_pkey PRIMARY KEY (id);


--
-- Name: inspections inspections_limit_inspectors; Type: TRIGGER; Schema: public; Owner: andy
--

CREATE TRIGGER inspections_limit_inspectors BEFORE INSERT ON public.inspections FOR EACH ROW EXECUTE PROCEDURE public.inspections_count_inspectors();


--
-- Name: inspections inspections_read_only_score; Type: TRIGGER; Schema: public; Owner: andy
--

CREATE TRIGGER inspections_read_only_score BEFORE UPDATE ON public.inspections FOR EACH ROW EXECUTE PROCEDURE public.inspections_compare_score();


--
-- Name: inspections inspections_verify_prerequisites; Type: TRIGGER; Schema: public; Owner: andy
--

CREATE TRIGGER inspections_verify_prerequisites BEFORE INSERT ON public.inspections FOR EACH ROW EXECUTE PROCEDURE public.inspections_check_prerequisites();


--
-- Name: builders builders_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.builders
    ADD CONSTRAINT builders_address_id_fkey FOREIGN KEY (address_id) REFERENCES public.addresses(id);


--
-- Name: inspection_sites inspection_sites_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspection_sites
    ADD CONSTRAINT inspection_sites_address_id_fkey FOREIGN KEY (address_id) REFERENCES public.addresses(id);


--
-- Name: inspection_sites inspection_sites_builder_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspection_sites
    ADD CONSTRAINT inspection_sites_builder_id_fkey FOREIGN KEY (builder_id) REFERENCES public.builders(license);


--
-- Name: inspections inspections_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspections
    ADD CONSTRAINT inspections_address_id_fkey FOREIGN KEY (address_id) REFERENCES public.addresses(id);


--
-- Name: inspections inspections_inspector_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspections
    ADD CONSTRAINT inspections_inspector_id_fkey FOREIGN KEY (inspector_id) REFERENCES public.inspectors(id);


--
-- Name: inspections inspections_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.inspections
    ADD CONSTRAINT inspections_type_fkey FOREIGN KEY (type) REFERENCES public.inspection_types(code);


--
-- Name: pending_inspections pending_inspections_inspection_site_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.pending_inspections
    ADD CONSTRAINT pending_inspections_inspection_site_id_fkey FOREIGN KEY (inspection_site_id) REFERENCES public.inspection_sites(id);


--
-- Name: pending_inspections pending_inspections_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: andy
--

ALTER TABLE ONLY public.pending_inspections
    ADD CONSTRAINT pending_inspections_type_fkey FOREIGN KEY (type) REFERENCES public.inspection_types(code);


--
-- PostgreSQL database dump complete
--

\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-1.pgdg90+1)
-- Dumped by pg_dump version 10.5 (Debian 10.5-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: andy
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-1.pgdg90+1)
-- Dumped by pg_dump version 10.5 (Debian 10.5-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: andy
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

