
-- -------------------------------------
-- Table Definitions
-- -------------------------------------

CREATE TABLE Inspection_Types (
	code VARCHAR(3) PRIMARY KEY,
	description TEXT NOT NULL,
	prerequisites TEXT[] NOT NULL,
	cost INTEGER NOT NULL
);

-- `valid_id` constraint ensures that the ID value is at most 5 digits
CREATE TABLE Inspectors (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL,
	hire_date DATE NOT NULL,
	CONSTRAINT valid_id CHECK (id BETWEEN 00000 AND 99999)
);

CREATE TABLE Addresses (
	id SERIAL PRIMARY KEY UNIQUE,
	street_address TEXT NOT NULL,
	city TEXT NOT NULL,
	state TEXT NOT NULL,
	zip_code VARCHAR(10) NOT NULL
);

-- since the license # is the primary key, it automatically has a UNIQUE constraint
--
-- the license # will not include leading zeroes--it is a numeric type in
-- the range of 10000-99999
--
-- address_id is a foreign key referencing an entry from the Addresses table
CREATE TABLE Builders (
	license INTEGER PRIMARY KEY UNIQUE,
	name TEXT NOT NULL,
	address_id INTEGER NOT NULL,
	FOREIGN KEY (address_id) REFERENCES Addresses (id),
	CONSTRAINT valid_license CHECK (license BETWEEN 10000 AND 99999)
);

CREATE TABLE Inspection_Sites (
	id SERIAL PRIMARY KEY UNIQUE,
	builder_id INTEGER NOT NULL,
	address_id INTEGER NOT NULL,
	type TEXT NOT NULL,
	size INTEGER,
	first_activity_date DATE,
	FOREIGN KEY (builder_id) REFERENCES Builders (license),
	FOREIGN KEY (address_id) REFERENCES Addresses (id)
);

CREATE TABLE Pending_Inspections (
	id SERIAL PRIMARY KEY UNIQUE,
	inspection_date DATE NOT NULL,
	type VARCHAR(3) NOT NULL,
	inspection_site_id INTEGER NOT NULL,
	FOREIGN KEY (type) REFERENCES Inspection_Types (code),
	FOREIGN KEY (inspection_site_id) REFERENCES Inspection_Sites (id)
);

CREATE TABLE Inspections (
	id SERIAL PRIMARY KEY UNIQUE,
	inspection_date DATE NOT NULL,
	type VARCHAR(3) NOT NULL,
	inspector_id INTEGER NOT NULL,
	score INTEGER NOT NULL,
	notes TEXT DEFAULT NULL,
	address_id INTEGER NOT NULL,
	FOREIGN KEY (type) REFERENCES Inspection_Types (code),
	FOREIGN KEY (inspector_id) REFERENCES Inspectors (id),
	FOREIGN KEY (address_id) REFERENCES Addresses (id),
	CONSTRAINT valid_score CHECK (score BETWEEN 0 AND 100)
);

-- -------------------------------------
-- Trigger and Procedure Definitions
-- -------------------------------------

-- given requirements:
--
-- "Some inspections cannot be performed before other inspections"
-- "The textual information [of an inspection] can be updated later, but the score can never be changed"
-- "[Inspectors]...can only perform at most 5 inspections per month"
-- "Any failed inspection can be repeated until passed"
-- "A request for an inspection may be assigned to any available inspector assuming the prerequisite inspections have a pass status."

-- this function restricts updates of inspection scores after they've
-- been inserted
CREATE OR REPLACE FUNCTION inspections_compare_score() RETURNS trigger AS $$

	BEGIN

		IF (NEW.score = OLD.score) THEN
			RETURN NEW;
		END IF;
		
		RAISE EXCEPTION 'Updates to inspection score forbidden. <INSPECTIONS - %>', NEW;

	END;

$$ LANGUAGE plpgsql;

CREATE TRIGGER inspections_read_only_score
BEFORE UPDATE ON Inspections
FOR EACH ROW EXECUTE PROCEDURE inspections_compare_score();

-- this function checks whether an attempted inspector has already
-- performed 5 inspections in the given month
CREATE OR REPLACE FUNCTION inspections_count_inspectors() RETURNS trigger AS $$

	DECLARE
		inspection_count INTEGER;

	BEGIN

		inspection_count := (
			SELECT COUNT(inspector_id)
			FROM inspections
			WHERE inspector_id = NEW.inspector_id AND
			EXTRACT(month FROM inspection_date) = EXTRACT(month FROM NEW.inspection_date)
		);

		IF (inspection_count < 5) THEN
			RETURN NEW;
		END IF;

		RAISE NOTICE 'Inspectors may not perform more than five (5) inspections in a given month. <INSPECTIONS - %>', NEW;
		RETURN NULL;

	END;

$$ LANGUAGE plpgsql;

CREATE TRIGGER inspections_limit_inspectors
BEFORE INSERT ON Inspections
FOR EACH ROW EXECUTE PROCEDURE inspections_count_inspectors();

-- this function checks whether an inspection has the proper prerequisites:
--  - iterates through list of prereqs
--  - for each prereq, does that prereq appear in insp_list for the same insp_site?
--  - if no, fail; if yes, did that inspection pass?
--  - if no, fail; if yes, RETURN NEW
CREATE OR REPLACE FUNCTION inspections_check_prerequisites() RETURNS trigger AS $$

	DECLARE
		-- variables in order of appearance
		prereqs TEXT[];
		prereq TEXT; -- loop variable
		past_prereq_score INTEGER;
		prev_inspection_score INTEGER;

	BEGIN

		-- Prescribed prerequisites
		-- 
		-- Type: TEXT array
		prereqs := (
			SELECT prerequisites
			FROM inspection_types
			WHERE code = NEW.type
		);

		-- if there are no prerequisites, then proceed with insertion
		IF (array_length(prereqs,1) IS NULL) THEN
			RETURN NEW;
		END IF;

		-- otherwise...
		-- first, check for a passing score from previous prerequisite inspections
		FOREACH prereq IN ARRAY prereqs
		LOOP

			-- determine latest score from given prerequisite
			past_prereq_score := (
				SELECT score
				FROM inspections
				WHERE
					address_id = NEW.address_id AND
					type = prereq
				ORDER BY inspection_date DESC
				LIMIT 1
			);

			-- if the query returned nothing, then the prereq has not been fulfilled
			IF (past_prereq_score IS NULL) THEN
				RAISE NOTICE 'Prerequisite % has not been performed. <INSPECTIONS - %>', prereq, NEW;
				RETURN NULL;
			END IF;

			-- if the query returned something, but it has a score below the threshold,
			-- the prereq inspection must be repeated until passed, and the currently
			-- requested inspection may not proceed
			IF (past_prereq_score < 75) THEN
				RAISE NOTICE 'Prerequisite % has not been passed. Latest score is %. <INSPECTIONS - %>', prereq, past_prereq_score, NEW;
				RETURN NULL;
			END IF;

		END LOOP;

		-- next, check if requested inspection type has been done before
		-- 		if it's already been done and has a passing score, don't allow a repeat
		--		otherwise, proceed with insertion
		prev_inspection_score := (
			SELECT score
			FROM inspections
			WHERE
				address_id = NEW.address_id AND
				type = NEW.type
			ORDER BY inspection_date DESC
			LIMIT 1
		);
		IF (prev_inspection_score >= 75) THEN
			RAISE NOTICE 'Given inspection has already been passed. <INSPECTIONS - %>', NEW;
			RETURN NULL;
		END IF;

		RETURN NEW;

	END;

$$ LANGUAGE plpgsql;

CREATE TRIGGER inspections_verify_prerequisites
BEFORE INSERT ON Inspections
FOR EACH ROW EXECUTE PROCEDURE inspections_check_prerequisites();

-- -------------------------------------
-- Insertions
-- -------------------------------------

INSERT INTO Inspection_Types
	VALUES ('FRM','Framing','{}',100),
		('PLU','Plumbing','{"FRM"}',100),
		('POL','Pool','{"PLU"}',50),
		('ELE','Electrical','{"FRM"}',100),
		('SAF','Safety','{}',50),
		('HAC','Heating/Cooling','{"ELE"}',100),
		('FNL','Final','{"HAC","PLU"}',200),
		('FN2','Final - 2 needed','{"ELE","PLU"}',150),
		('FN3','Final - plumbing','{"PLU"}',150),
		('HIS','Historical accuracy','{}',100);

INSERT INTO Inspectors
	VALUES (101,'Inspector-1','1984-11-08'),
		(102,'Inspector-2','1994-11-08'),
		(103,'Inspector-3','2004-11-08'),
		(104,'Inspector-4','2014-11-01'),
		(105,'Inspector-5','2018-11-01');

INSERT INTO Addresses (street_address, city, state, zip_code)
	VALUES ('100 Winding Wood','Carrollton','TX','75006'),
		('308 Oak St.','Dallas','TX','75219'),
		('306 Oak St.','Dallas','TX','75219'),
		('212 Cherry Bark Lane','Plano','TX','75074'),
		('100 Industrial Ave.','Fort Worth','TX','76104'),
		('103 Industrial Ave.','Fort Worth','TX','76104'),
		('104 Industrial Ave.','Fort Worth','TX','76104'),
		('304 Oak St.','Dallas','TX','75219'),
		('100 Main St.','Dallas','TX','75226'),
		('102 Winding Wood','Carrollton','TX','75006'),
		('300 Oak St.','Dallas','TX','75219'),
		('216 Cherry Bark Lane','Plano','TX','75074'),
		('302 Oak St.','Dallas','TX','75219'),
		('101 Industrial Ave.','Fort Worth','TX','76104'),
		('210 Cherry Bark Lane','Plano','TX','75074'),
		('105 Industrial Ave.','Fort Worth','TX','76104'),
		('214 Cherry Bark Lane','Plano','TX','75074'),
		('102 Industrial Ave.','Fort Worth','TX','76104'),
		('6300 N Central Expy.','Dallas','TX','75206'),
		('1728 Briercroft Ct.','Carrollton','TX','75006'),
		('788 E Collins Blvd.','Richardson','TX','75081'),
		('1901 Regal Row','Dallas','TX','75235'),
		('5101 Maple Ave.','Dallas','TX','75235');

INSERT INTO Builders
	VALUES (12345,'Builder-1',21),
		(23456,'Builder-2',23),
		(34567,'Builder-3',22),
		(45678,'Builder-4',19),
		(12321,'Builder-5',20);

INSERT INTO Inspection_Sites (builder_id,address_id,type,size,first_activity_date)
	VALUES (12345,9,'commercial',250000,'1999-12-31'),
		(12345,11,'residential',3000,'2000-01-01'),
		(12345,13,'residential',4000,'2001-02-01'),
		(12345,8,'residential',1500,'2002-03-01'),
		(12345,3,'residential',1500,'2003-04-01'),
		(12345,2,'residential',2000,'2003-04-01'),
		(23456,5,'commercial',100000,'2005-06-01'),
		(23456,14,'commercial',80000,'2005-06-01'),
		(23456,18,'commercial',75000,'2005-06-01'),
		(23456,6,'commercial',50000,'2005-06-01'),
		(23456,7,'commercial',80000,'2005-06-01'),
		(23456,16,'commercial',90000,'2005-06-01'),
		(45678,1,'residential',2500,NULL),
		(45678,10,'residential',2800,NULL),
		(12321,15,'residential',3200,'2016-10-01'),
		(12321,4,'residential',NULL,NULL),
		(12321,17,'residential',NULL,NULL),
		(12321,12,'residential',NULL,NULL);

INSERT INTO Pending_Inspections (inspection_date,type,inspection_site_id)
	VALUES ('2018-09-01','FNL',16),
		('2018-10-26','FRM',4),
		('2018-11-04','PLU',4);

INSERT INTO Inspections (inspection_date,type,inspector_id,score,notes,address_id)
	VALUES ('2018-10-01','FRM',103,100,'no problems noted',1),
		('2018-10-01','FRM',101,100,'no problems noted',11),
		('2018-10-01','FRM',102,100,'no problems noted',13),
		('2018-10-02','PLU',101,90,'minor leak, corrected',11),
		('2018-10-02','PLU',102,25,'massive leaks',13),
		('2018-10-03','ELE',101,80,'exposed junction box',11),
		('2018-10-04','HAC',101,80,'duct needs taping',11),
		('2018-10-05','FNL',101,90,'ready for owner',11),
		('2018-10-08','PLU',102,50,'still leaking',13),
		('2018-10-12','FRM',103,85,'no issues but messy',15),
		('2018-10-12','PLU',102,80,'no leaks, but messy',13),
		('2018-10-14','ELE',104,100,'no problems noted',15),
		('2018-10-14','SAF',102,100,'no problems noted',13),
		('2018-10-20','PLU',103,100,'everything working',1),
		('2018-10-25','ELE',103,100,'no problems noted',1),
		('2018-11-01','FRM',103,100,'no problems noted',10),
		('2018-11-01','HAC',102,80,'duct needs taping',13),
		('2018-11-02','HAC',103,100,'no problems noted',1),
		('2018-11-02','PLU',103,90,'minor leak, corrected',10),
		('2018-11-02','FRM',105,100,'tbd',16),
		('2018-11-02','FNL',102,90,'ready for owner',13),
		('2018-11-03','ELE',103,80,'exposed junction box',10),
		('2018-11-04','PLU',103,80,'duct needs sealing',15),
		('2018-11-05','POL',105,90,'ready for owner',15),
		('2018-11-06','FRM',105,100,'okay',5),
		('2018-11-08','PLU',102,100,'no leaks',5),
		('2018-11-12','POL',102,80,'pool equipment okay',5),
		('2018-11-14','FN3',103,100,'no problems noted',5),
		('2018-11-14','FNL',103,90,'REJECT TOO MANY',1);
