# Semester Project - Part 3

Robert Rash  
CSE 5330  
3 December 2018

The following document contains responses to the requests outlined in the Project - Part 3 assignment. Each query below is matched with the output from running said query in the PostgreSQL command line environment. However, since these are only text responses (i.e. they can be mocked up / faked), additional proof can be found in the SQL dump file that is attached with the assignment.

## Description Queries

> List all buildings (building#, address, type) that have not passed a final (FNL, FN2, FN3) inspection.

### Query

The following query will return the requested values if they exist in the database. Given the data that was provided, there are no final inspections that have failed, so this query returns nothing.

```sql
SELECT
	inspections.id AS inspection_id,
	inspections.inspection_date,
	inspections.type AS inspection_type,
	addresses.id AS building_no,
	addresses.street_address,
	addresses.city,
	addresses.state,
	inspection_sites.type AS building_type
FROM inspections
INNER JOIN inspection_sites ON inspections.address_id = inspection_sites.address_id
INNER JOIN addresses ON inspections.address_id = addresses.id
WHERE inspections.score < 75 AND
	(
		inspections.type = 'FNL' OR
		inspections.type = 'FN2' OR
		inspections.type = 'FN3'
	);
```

### Result

```
 inspection_id | inspection_date | inspection_type | building_no | street_address | city | state | building_type
---------------+-----------------+-----------------+-------------+----------------+------+-------+---------------
(0 rows)
```

> List the id, name of inspectors who have given at least one failing score.

### Query

```sql
SELECT
	inspectors.id AS inspector_ID,
	inspectors.name AS inspector_name
FROM inspections
INNER JOIN inspectors on inspections.inspector_id = inspectors.id
WHERE inspections.score < 75
GROUP BY 1;
```

### Result

```
 inspector_id | inspector_name
--------------+----------------
          102 | Inspector-2
(1 row)
```

> What inspection type(s) have never been failed?

### Query

```sql
SELECT inspection_types.code
FROM inspection_types
EXCEPT
(
	SELECT DISTINCT
		inspection_types.code
	FROM inspection_types
	LEFT JOIN inspections ON inspection_types.code = inspections.type
	WHERE inspections.score < 75
);
```

### Result

```
 code
------
 FN2
 HIS
 FN3
 SAF
 FNL
 POL
 HAC
 ELE
(8 rows)
```

> What is the total cost of all inspections for builder 12345?

### Query

```sql
SELECT
	SUM(inspection_types.cost) AS total_cost
FROM inspection_sites
INNER JOIN inspections ON inspection_sites.address_id = inspections.address_id
INNER JOIN inspection_types ON inspections.type = inspection_types.code
WHERE inspection_sites.builder_id = 12345;
```

### Result

```
 total_cost
------------
       1050
(1 row)
```

> What is the average score for all inspections performed by Inspector 102?

### Query

```sql
SELECT
	AVG(inspections.score)
FROM inspections
WHERE inspections.inspector_id = 102;
```

Since the result is not an integer, the DB returns a floating point value. If an integer type is preferred, the query can be altered to cast the `AVG` as an integer:

```sql
SELECT
	AVG(inspections.score)::integer
FROM inspections
WHERE inspections.inspector_id = 102;
```

### Result

**Floating Point Result**

```
         avg
---------------------
 76.4285714285714286
(1 row)
```

**Integer Result**

```
 avg
-----
  76
(1 row)
```

> How much revenue did FODB receive for inspections during October 2018?

### Query

```sql
SELECT
	SUM(inspection_types.cost) AS "October Revenue"
FROM inspections
INNER JOIN inspection_types ON inspections.type = inspection_types.code
WHERE EXTRACT(month FROM inspections.inspection_date) = 10;
```

### Result

```
 October Revenue
-----------------
            1550
(1 row)
```

> How much revenue was generated in 2018 by inspectors with more than 15 years seniority?

### Query

```sql
SELECT
	SUM(inspection_types.cost) AS "Revenue, 15 year employees"
FROM inspectors
INNER JOIN inspections ON inspectors.id = inspections.inspector_id
INNER JOIN inspection_types ON inspections.type = inspection_types.code
WHERE inspectors.hire_date < now() - interval '15 years';
```

### Result

```
 Revenue, 15 year employees
----------------------------
                       1200
(1 row)
```

## Manipulation Queries

> Demonstrate the adding of a new 1600 sq ft residential building for builder #34567, located at 1420 Main St., Lewisville TX.

### Query

To achieve the requested with the way this database is set up, there must be two statements, one to create the `Address` entry and an entry in `Inspection_Sites` to contain information such as square footage, etc.

As well, since addresses are split into their individual parts, the address `1420 Main St., Lewisville TX` will be split into `1420 Main St.`, `Lewisville`, and `TX`, along with an added ZIP code field, which for this address turns out to be `75067`.

**Create `Address` entry**

```sql
INSERT INTO addresses (street_address,city,state,zip_code)
	VALUES ('1420 Main St.','Lewisville','TX','75067');
```

**Create `Inspection_Site` entry**

```sql
INSERT INTO inspection_sites (builder_id,address_id,type,size,first_activity_date)
	SELECT 34567,id,'residential',1600,NULL
	FROM addresses
	WHERE
		street_address = '1420 Main St.' AND
		city = 'Lewisville' AND
		state = 'TX';
```

### Result

```
FODB=# SELECT
FODB-#  inspection_sites.id,
FODB-#  inspection_sites.builder_id,
FODB-#  addresses.street_address,
FODB-#  addresses.city,
FODB-#  addresses.state,
FODB-#  addresses.zip_code,
FODB-#  inspection_sites.type,
FODB-#  inspection_sites.size,
FODB-#  inspection_sites.first_activity_date
FODB-# FROM inspection_sites
FODB-# INNER JOIN addresses on inspection_sites.address_id = addresses.id
FODB-# WHERE
FODB-#  addresses.street_address = '1420 Main St.' AND
FODB-#  addresses.city = 'Lewisville' AND
FODB-#  addresses.state = 'TX';

 id | builder_id | street_address |    city    | state | zip_code |    type     | size | first_activity_date
----+------------+----------------+------------+-------+----------+-------------+------+---------------------
 19 |      34567 | 1420 Main St.  | Lewisville | TX    | 75067    | residential | 1600 |
(1 row)
```

> Demonstrate the adding of an inspection on the building you just added. This framing inspection occurred on 11/21/2018 by inspector 104, with a score of 50, and note of “work not finished.”

### Query

```sql
INSERT INTO inspections (inspection_date,type,inspector_id,score,notes,address_id)
	SELECT '2018-11-21','FRM',104,50,'work not finished',id
	FROM addresses
	WHERE
		street_address = '1420 Main St.' AND
		city = 'Lewisville' AND
		state = 'TX';
```

### Result

```
FODB=# SELECT *
FODB-# FROM inspections
FODB-# WHERE
FODB-# inspection_date = '2018-11-21' AND
FODB-#  type = 'FRM' AND
FODB-#  inspector_id = 104 AND
FODB-#  address_id = (
FODB(#   SELECT id
FODB(#   FROM addresses
FODB(#   WHERE
FODB(#
city            id              state           street_address  zip_code
FODB(#
city            id              state           street_address  zip_code
FODB(# street_address = '1420 Main St.' AND
FODB(#   city = 'Lewisville' AND
FODB(#   state = 'TX'
FODB(#  );

 id | inspection_date | type | inspector_id | score |       notes       | address_id
----+-----------------+------+--------------+-------+-------------------+------------
 30 | 2018-11-21      | FRM  |          104 |    50 | work not finished |         24
(1 row)
```

> Demonstrate changing the cost of an ELE inspection changed to $150 effective today.

### Query

To track historical price updates--or updates to other fields in the `Inspection_Types` table, for that matter--it becomes necessary to create a `Inspection_Types_History` table that, along with a timestamp, mirrors changes to the `Inspection_Types` table through an associated trigger.

```sql
-- schema definition
CREATE TABLE Inspection_Types_History (
	code VARCHAR(3),
	effective_date TIMESTAMP WITH TIME ZONE NOT NULL,
	description TEXT NOT NULL,
	prerequisites TEXT[] NOT NULL,
	cost INTEGER NOT NULL,
	PRIMARY KEY (code,effective_date)
);

-- procedure definition
CREATE OR REPLACE FUNCTION inspection_types_audit() RETURNS trigger AS $$

	BEGIN

		INSERT INTO inspection_types_history
			(
				code,
				effective_date,
				description,
				prerequisites,
				cost
			)
			VALUES (
				OLD.code,
				now(),
				OLD.description,
				OLD.prerequisites,
				OLD.cost
			);

		RETURN NEW;

	END;

$$ LANGUAGE plpgsql;

-- trigger definition
CREATE TRIGGER inspection_types_perform_audit
BEFORE UPDATE ON Inspection_Types
FOR EACH ROW EXECUTE PROCEDURE inspection_types_audit();

```

Now that that's done, a simple `UPDATE` query gets the job done:

```sql
UPDATE inspection_types
SET cost = 150
WHERE code = 'ELE';
```

Of course, this solution creates some level of technical debt if, for example, a row is deleted, if the schema changes, or if this functionality needs to be extended to other tables. However, for the purposes of this project, it's an acceptable solution.

### Result

**Updated value**

```
FODB=# SELECT * FROM inspection_types WHERE code = 'ELE';

 code | description | prerequisites | cost
------+-------------+---------------+------
 ELE  | Electrical  | {FRM}         |  150
(1 row)
```

**Historical value**

```
FODB=# SELECT * FROM inspection_types_history;

 code |        effective_date         | description | prerequisites | cost
------+-------------------------------+-------------+---------------+------
 ELE  | 2018-12-04 04:42:04.643213+00 | Electrical  | {FRM}         |  100
(1 row)
```

> Demonstrate adding of an inspection on the building you just added. This electrical inspection occurred on 11/22/2018 by inspector 104, with a score of 60, and note of “lights not completed.”

### Query

```sql
INSERT INTO inspections (inspection_date,type,inspector_id,score,notes,address_id)
	SELECT '2018-11-22','ELE',104,60,'lights not completed',id
	FROM addresses
	WHERE
		street_address = '1420 Main St.' AND
		city = 'Lewisville' AND
		state = 'TX';
```

### Result

This insertion fails because the prerequisite framing inspection has not been passed.

```
FODB=# INSERT INTO inspections (inspection_date,type,inspector_id,score,notes,address_id)
FODB-# SELECT '2018-11-22','ELE',104,60,'lights not completed',id
FODB-#  FROM addresses
FODB-#  WHERE
FODB-#
city            id              state           street_address  zip_code
FODB-# street_address = '1420 Main St.' AND
FODB-#   city = 'Lewisville' AND
FODB-#   state = 'TX';

NOTICE:  Prerequisite FRM has not been passed. Latest score is 50. <INSPECTIONS - (31,2018-11-22,ELE,104,60,"lights not completed",24)>
INSERT 0 0
```

> Demonstrate changing the message of the FRM inspection on 11/2/2018 by inspector #105 to “all work completed per checklist.”

### Query

```sql
UPDATE inspections
SET notes = 'all work completed per checklist.'
WHERE
	inspection_date = '2018-11-02' AND
	type = 'FRM' AND
	inspector_id = 105;
```

### Result

```
FODB=# SELECT *
FODB-# FROM inspections
FODB-# WHERE
FODB-# inspection_date = '2018-11-02' AND
FODB-#  type = 'FRM' AND
FODB-#  inspector_id = 105;

 id | inspection_date | type | inspector_id | score |               notes               | address_id
----+-----------------+------+--------------+-------+-----------------------------------+------------
 20 | 2018-11-02      | FRM  |          105 |   100 | all work completed per checklist. |         16
(1 row)
```

> Demonstrate the adding of a POL inspection by inspector #103 on 11/28/2018 on the first building associated with builder 45678.

### Query

The provided request does not include a score. Since a statement without this value will _always_ fail due to the schema, I have provided my own value. Since no notes were provided, this will be entered as an empty string--this can always be updated later, as the previous statement shows.

```sql
INSERT INTO inspections (inspection_date,type,inspector_id,score,notes,address_id)
	SELECT '2018-11-28','POL',103,74,'',addresses.id
	FROM addresses
	INNER JOIN inspection_sites ON addresses.id = inspection_sites.address_id
	WHERE
		inspection_sites.builder_id = 45678
	LIMIT 1;
```

### Result

Despite the precautions outlined above, unfortunately, this query fails due to the fact that the inspector has already reached his quota for inspections.

```
FODB=# INSERT INTO inspections (inspection_date,type,inspector_id,score,notes,address_id)
FODB-# SELECT '2018-11-28','POL',103,74,'',addresses.id
FODB-#  FROM addresses
FODB-#  INNER JOIN inspection_sites ON addresses.id = inspection_sites.address_id
FODB-#  WHERE
FODB-#   inspection_sites.builder_id = 45678
FODB-#  LIMIT 1;

NOTICE:  Inspectors may not perform more than five (5) inspections in a given month. <INSPECTIONS - (32,2018-11-28,POL,103,74,"",1)>
INSERT 0 0
```
