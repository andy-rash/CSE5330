from sqlalchemy import (
	Column, Integer, String, ForeignKey
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Course(Base):

	__tablename__ = 'courses'
	course_name = Column(String, nullable=False)
	course_number = Column(String, primary_key=True, nullable=False)
	credit_hours = Column(Integer, nullable=False)
	department = Column(String, nullable=False)

	sections = relationship('Section', backref='courses')
	prereq_course_number = relationship('Prerequisite')
	prereq_prereq_number = relationship('Prerequisite')

	def __init__(self, **kwargs):
		super(Course, self).__init__(**kwargs)

	def __repr__(self):
		return f'<Course - {self.course_number}>'

class GradeReport(Base):

	__tablename__ = 'grade_reports'
	student_number = Column(Integer, primary_key=True, nullable=False)
	section_identifier = Column(Integer, primary_key=True, nullable=False)
	grade = Column(String, nullable=False)

	# foreign key student_number references Student().student_number
	# foreign key section_identifier references Section().section_identifier

	def __init__(self, **kwargs):
		super(GradeReport, self).__init__(**kwargs)

	def __repr__(self):
		return f'<GradeReport - {self.student_number}, {self.section_identifier}>'

class Prerequisite(Base):

	__tablename__ = 'prerequisites'
	course_number = Column(String, primary_key=True, ForeignKey('courses.course_number'))
	prerequisite_number = Column(String, primary_key=True, ForeignKey('courses.course_number'))

	# foreign key course_number references Course().course_number
	# foreign key prerequisite_number references Course().course_number

	def __init__(self, **kwargs):
		super(Prerequisite, self).__init__(**kwargs)

	def __repr__(self):
		return f'<Prerequisite - {self.prerequisite_number}>'

class Section(Base):

	__tablename__ = 'sections'
	section_identifier = Column(Integer, primary_key=True, nullable=False)
	course_number = Column(String, ForeignKey('courses.course_number'))
	semester = Column(String, nullable=False)
	year = Column(Integer, nullable=False)
	instructor = Column(String, default='TBD')

	def __init__(self, **kwargs):
		super(Section, self).__init__(**kwargs)

	def __repr__(self):
		return f'<Section - {self.section_identifier}, {self.course_number}>'

class Student(Base):

	__tablename__ = 'students'
	name = Column(String, nullable=False)
	student_number = Column(Integer, primary_key=True, nullable=False)
	student_class = Column(Integer, nullable=False, default=1)
	major = Column(String, nullable=False, default='None')

	def __init__(self, **kwargs):
		super(Student, self).__init__(**kwargs)

	def __repr__(self):
		return f'<Student - {self.name}>'
