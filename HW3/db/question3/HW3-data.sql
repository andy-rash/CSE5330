-- CSE 5/7330 Fall 2018 Homework 3, data for question 3
--

INSERT INTO Students VALUES (8,'Brown','CS',2),(17,'Smith','CS',1),(33,'Jones','MATH',2),(42,'Alias','CS',1),(58,'Newbie','CS',1);

INSERT INTO Courses VALUES ('CS1310','Intro to Computer Science',4,'CS'),('CS3320','Data Structures',4,'CS'),('CS3380','Database',3,'CS'),('MATH2100','Basic Algebra',4,'MATH'),('MATH2410','Discrete Mathematics',3,'MATH');

INSERT INTO Prerequisites VALUES ('CS3320','CS1310'),('CS3380','CS3320'),('CS3380','MATH2410'),('MATH2410','MATH2100');

INSERT INTO Sections VALUES (85,17,'Fall','King','MATH2410'),(92,17,'Fall','Anderson','CS1310'),(102,18,'Spring','Knuth','CS3320'),(112,18,'Fall','Chang','MATH2410'),(119,18,'Fall','Anderson','CS1310'),(120,18,'Fall','Andersen','CS1310'),(135,18,'Fall','Stone','CS3380');

INSERT INTO Grade_Reports VALUES ('A',8,85),('A',8,92),('B',8,102),('A',8,135),('B',17,112),('C',17,119),('F',33,85),('A',33,112),('B',42,102),('B',42,135);

------------------------

INSERT INTO Employees VALUES (100,'John Doe','CEO'),(111,'Sally Financial','CFO'),(122,'HR Office','HR'),(133,'T Produce','Mgr'),(144,'Sam Shipper','Mgr');

INSERT INTO Documents VALUES (1,'external-report',60,'2018 Annual Report'),(2,'memo',1,'2018 Goals'),(3,'external-report',2,'2018 OSHA Report'),(4,'internal-report',5,'preliminary OSHA response'),(5,'external-report',10,'Gov OSHA Response'),(6,'internal-report',25,'Draft financial report'),(7,'external-report',10,'Gov Req CFO report'),(8,'memo',2,'Shipping requirements');

INSERT INTO Contributions VALUES (100,1,100),(100,2,50),(111,2,25),(122,2,25),(100,3,50),(133,3,50),(133,4,100),(133,5,100),(111,6,100),(111,7,100),(144,8,100);
