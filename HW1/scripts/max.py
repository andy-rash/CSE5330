import time

file_path = "../data/n100000.csv"

# avg_25k = 0.0
# avg_50k = 0.0
# avg_100k = 0.0

# for i in range(0,100):
# 	n = []
# 	start = time.time()
# 	with open(file_path) as f:
# 		for l in f:
# 			n.append(int(l,10))

# 	x = max(n[0:25000])
# 	avg_25k += time.time() - start

# 	y = max(n[0:50000])
# 	avg_50k += time.time() - start

# 	z = max(n)
# 	avg_100k += time.time() - start


# print(avg_25k / 100,avg_50k / 100,avg_100k / 100)

# ---------------

avg_25k = 0.0
avg_50k = 0.0
avg_100k = 0.0

for i in range(0,100):
	line = 0
	max_int = 0
	start = time.time()
	with open(file_path) as f:
		for l in f:

			if int(l,10) >= max_int:
				max_int = int(l,10)

			line += 1

			if line == 24999:
				avg_25k += time.time() - start
			elif line == 49999:
				avg_50k += time.time() - start
			elif line == 100000:
				avg_100k += time.time() - start

print(avg_25k / 100,avg_50k / 100,avg_100k / 100)
